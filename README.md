# [depredcated] USE https://github.com/sung/TSM

## Introduction
This package, TSM (aka. The Smith Method), is to select a desired number of features (4 by default) by purposefully dropping highly correlated ones, *i.e,* picking up highly representative features that can best explain the binary outcomes. In plain language, it works like the follwoing: The first representative feature is the one that shows the highest AUC (Area Under the ROC Curve) out of all the features. The next representative feature is the one that shows the highest AUC out of the remaing features after dropping highly correlated features with the first representative feature. The third, the fourth, and so on, represenative feature will be picked up as the same way the 2nd is picked up.

By default, `spearman` correlation coefficient is used to check the correlation among the possible features and they will be dropped if they are above a certain threshold (e.g. 0.5) by leaving a represenative one having the best AUC. It recursively check their correlations and drop the feature until nothing left to check. By default, the thresholds for the correlation coefficients are set from 0.1 (i.e. highly stringent by leaving less features) to 0.7 (i.e. less stringent by leaving more features) by increasing 0.1 at each step. 

## How to use
+ Source the code:
```r
  source("TSM.R")
```

+ Read the input, e.g:
```r
input<-read.csv("TSM_input.csv")
```
NB, there should be a column (`y` by default) which contains a binary outcome, either `0` or `1`. 

+ Finally, run the code.
```r
TSM(x=input)
```

This will return a table (`data.table`) by descending order of Leave-Pair-Out-Cross-Validation (LPOCV) [Gordon Am J Epi 2014](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4108045/) at each threshold of correlation coefficients.

## Parameters
+ In case you'd like only 0.4 and 0.5 correlation coefficients and only 3 features:
```r
TSM(x=input,corr=c(0.4,0.5),k=3)
```

+ In addtion to the above, in case you'd like `pearson` correlation instead of `spearman` (default):
```r
TSM(x=input,method="pearson",corr=c(0.4,0.5),k=3)
```

+ In case you'd like more verbose version of outcomes:
```r
foo<-TSM(x=input,corr=c(0.4,0.5),k=3,verbose=T)

foo[["cor0.4"]]
foo[["cor0.5"]]
rbindlist(foo[["performance"]])[order(-`AUC(LPOCV)`)]
```

## Contact
Sung Gong <ssg29@cam.ac.uk>
